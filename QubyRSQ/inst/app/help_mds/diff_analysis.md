# Differential analysis

Differential analysis is done with edgeR is the shiny appliaction. edgeR provides statistical routines for assessing differential expression in RNA-Seq experiments. The package implements exact statistical methods for multigroup experiments developed by Robinson and Smyth. It also implements statistical methods based on generalized linear models (glms), suitable for multifactor experiments of any complexity, developed by McCarthy et al., Lund et al., Chen et al. and Lun et al. 


